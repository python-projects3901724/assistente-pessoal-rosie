import speech_recognition as sr
import os
import webbrowser as browser
from playsound import playsound
from requests import get
from bs4 import BeautifulSoup
from gtts import gTTS
from paho.mqtt import publish

##### LISTAGEM DOS COMANDOS #####
'''
NOTÍCIAS                    Últimas notícias
TOCA <NOME DO ÁLBUM>        Reproduz o álbum no spotify player web
TEMPO AGORA                 Informações sobre temperatura e condição Climática
TEMPERATURA HOJE            Informações sobre mínima e máxima
LIGA/DESATIVA BUNKER        Controla iluminação do escritório
'''

##### CONFIGURAÇÕES #####
hotword = 'rose'

##### FUNÇÕES PRINCIPAIS #####
def monitora_audio():
    microfone = sr.Recognizer()
    with sr.Microphone() as source:
        while True:
            print("Aguardando o Comando: ")
            audio = microfone.listen(source)
            try:
                trigger = microfone.recognize_google_cloud(audio, credentials_json='C:/workspace/python/audioProject/audioproject-390120-b3ca47ff3324.json', language='pt-BR')
                trigger = trigger.lower()

                if hotword in trigger:
                    print('Comando: ', trigger)
                    responde('feedback')
                    executa_comandos(trigger)
                    break

            except sr.UnknownValueError:
                print("Google not understand audio")
            except sr.RequestError as e:
                print("Could not request results from Google Cloud Speech service; {0}".format(e))
    return trigger

def responde(arquivo):
  playsound('C:/workspace/python/audioProject/song/' + arquivo + '.mp3')

def executa_comandos(trigger):
    if 'notícias' in trigger:
        ultimas_noticias()
    elif 'toca' in trigger and 'guardiões' in trigger:
        playlist('1')
    elif 'tempo agora' in trigger:
        previsao_tempo(tempo=True)
    elif 'temperatura hoje' in trigger:
        previsao_tempo(minmax=True)
    elif 'liga o bunker' in trigger:
        publica_mqtt('office/iluminacao/status', '1')

    elif 'desativa o bunker' in trigger:
        publica_mqtt('office/iluminacao/status', '0')
    else:
        msg = trigger.strip(hotword)
        cria_audio(msg)
        print('Comando Inválido')
        responde('mensagem2')

def publica_mqtt(topic, payload):
    publish.single(topic, payload=payload, qos=1, retain=True, hostname='m10.cloudmqtt.com', port=12892, client_id='rosie', auth={'username': 'xxxxxxxx', 'password': 'xxxxxxxx'})
    if payload == '1':
        mensagem = 'Bunker Ligado!'
    elif payload == '0':
        mensagem = 'Bunker Desligado!'
    cria_audio(mensagem)
def ultimas_noticias():
    site = get('https://news.google.com/rss?hl=pt-BR&gl=BR&ceid=BR:pt-419')
    noticias = BeautifulSoup(site.text, 'html.parser')
    for item in noticias.findAll('item')[:5]:
        mensagem = item.title.text
        cria_audio(mensagem)
        print(mensagem)

def cria_audio(mensagem):
    tts = gTTS(mensagem, lang='pt-br')
    tts.save('song/mensagem.mp3')
    print('ROSIE: ', mensagem)
    playsound('C:/workspace/python/audioProject/song/mensagem.mp3')

def previsao_tempo(tempo=False, minmax=False):
    site = get('http://api.openweathermap.org/data/2.5/weather?id=3451190&APPID=111111111111111&units=metric&lang=pt')
    clima = site.json()
    temperatura=clima['main']['temp']
    minima=clima['main']['temp_min']
    maxima=clima['main']['temp_max']
    descricao=clima['weather'][0]['description']
    if tempo:
        mensagem = f'No momento fazem {temperatura} graus com: {descricao}'
    elif minmax:
        mensagem = f'Mínima de {minima} e máxima de {maxima} graus'
    cria_audio(mensagem)

def playlist(play):
    if '1' == play:
        browser.open('https://open.spotify.com/playlist/6QNoOEEuDU2bxlAQ7WCnC9')

def main():
    while True:
        monitora_audio()

main()



